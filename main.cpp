#include <iostream>
#include <string>
#include "tree.h"
#include <sstream>
#include <fstream>
#include <vector>
using namespace std;

void main()
{
	Tree<string> mytree;
	vector<string>wordline;
	string word, subword;
	int number_line;
	//
	//Add text file
	ifstream file;
	file.open("word.txt");
	//
	//
	int line = 1;//start at line 1
	while (getline(file, word))
	{
		wordline.push_back(word);//push word in to vector name wordline
		stringstream sup(word);
		while (getline(sup, subword, ' '))
		{
			subword = subword + " " + to_string(line)+"\n";//show every word in txt file
			mytree.insert(subword);
		}line++;
	}
	mytree.inorder();
	cout << endl;
	cout << "Input line number to watch all word in the line:";
	cin >> number_line;
	cout << wordline.at(number_line - 1) << endl;
	system("Pause");
	
}